/*
This file is part of ftdock, a program for rigid-body protein-protein docking
Copyright (C) 1997-2000 Gidon Moont

Biomolecular Modelling Laboratory
Imperial Cancer Research Fund
44 Lincoln's Inn Fields
London WC2A 3PX

+44 (0)20 7269 3348
http://www.bmm.icnet.uk/

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "structures.h"
#include "emmintrin.h"

void assign_charges( struct Structure This_Structure ) {

/************/

  /* Counters */

  int	residue, atom ;

/************/

  for (residue = 1; residue <= This_Structure.length; residue++) {
    for (atom = 1; atom <= This_Structure.Residue[residue].size; atom++) {

      This_Structure.Residue[residue].Atom[atom].charge = 0.0;

      /* peptide backbone */

      if (strcmp(This_Structure.Residue[residue].Atom[atom].atom_name, " N  ") == 0) {
        if (strcmp(This_Structure.Residue[residue].res_name, "PRO") == 0) {
          This_Structure.Residue[residue].Atom[atom].charge = -0.10 ;
        } else {
          This_Structure.Residue[residue].Atom[atom].charge =  0.55 ;
          if (residue == 1) This_Structure.Residue[residue].Atom[atom].charge = 1.00 ;
        }
      }

      if (strcmp(This_Structure.Residue[residue].Atom[atom].atom_name, " O  ") == 0) {
        This_Structure.Residue[residue].Atom[atom].charge = -0.55 ;
        if (residue == This_Structure.length) This_Structure.Residue[residue].Atom[atom].charge = -1.00;
      }

      /* charged residues */

      if ((strcmp(This_Structure.Residue[residue].res_name, "ARG") == 0) && ( strncmp( This_Structure.Residue[residue].Atom[atom].atom_name, " NH", 3) == 0) ) This_Structure.Residue[residue].Atom[atom].charge =  0.50 ;
      if ((strcmp(This_Structure.Residue[residue].res_name, "ASP") == 0) && ( strncmp( This_Structure.Residue[residue].Atom[atom].atom_name, " OD", 3) == 0) ) This_Structure.Residue[residue].Atom[atom].charge = -0.50 ;
      if ((strcmp(This_Structure.Residue[residue].res_name, "GLU") == 0) && ( strncmp( This_Structure.Residue[residue].Atom[atom].atom_name, " OE", 3) == 0) ) This_Structure.Residue[residue].Atom[atom].charge = -0.50 ;
      if ((strcmp(This_Structure.Residue[residue].res_name, "LYS") == 0) && (strcmp(This_Structure.Residue[residue].Atom[atom].atom_name, " NZ ") == 0) ) This_Structure.Residue[residue].Atom[atom].charge =  1.00 ;

    }
  }

/************/

}



/************************/



void electric_field(struct Structure *This_Structure, float grid_span, int grid_size, fftwf_real *grid) {
	// Co-ordinates

	int x, y, z;
	float x_centre, y_centre, z_centre;

	// Variables
	float distance;
	float phi, epsilon;


	for (x = 0; x < grid_size; x++) {
		for (y = 0; y < grid_size; y++) {
			for (z = 0; z < grid_size; z++) {

				grid[gaddress(x,y,z,grid_size)] = (fftwf_real)0;

			}
		}
	}

	setvbuf(stdout, (char *)NULL, _IONBF, 0);
	printf("  electric field calculations ( one dot / grid sheet ) ");

	// Memoization
	float centers[grid_size];
	int i;
	for (i = 0; i < grid_size; i++)
		centers[i] = gcentre(i, grid_span, grid_size);

	const struct Amino_Acid *residue_end = This_Structure->Residue + This_Structure->length;
	struct Amino_Acid *residue = &This_Structure->Residue[1];

	const __m128 const_2_0 = _mm_set_ps1(2.0);
	const __m128 const_8_0 = _mm_set_ps1(8.0);
	const __m128 const_6_0 = _mm_set_ps1(6.0);
	const __m128 const_80 = _mm_set_ps1(80);
	const __m128 const_4 = _mm_set_ps1(4);
	const __m128 const_38 = _mm_set_ps1(38);
	const __m128 const_224 = _mm_set_ps1(224);
	const __m128 const_neg1 = _mm_castsi128_ps(_mm_set1_epi8(0xFF));

	for (; residue <= residue_end; residue++) {

		const struct Atom *atom_end = residue->Atom + residue->size;
		struct Atom *atom = &residue->Atom[1];

		for (; atom <= atom_end; atom++) {
			if (atom->charge != 0) {

				__m128 atom_coord1 = _mm_set_ps1(atom->coord[1]);
				__m128 atom_coord2 = _mm_set_ps1(atom->coord[2]);
				__m128 atom_coord3 = _mm_set_ps1(atom->coord[3]);
				__m128 atom_charge = _mm_set_ps1(atom->charge);

				for (x = 0; x < grid_size; x++) {
					__m128 x_centre = _mm_set_ps1(centers[x]);
					for (y = 0; y < grid_size; y++) {
						__m128 y_centre = _mm_set_ps1(centers[y]);
						//Prologue
						for (z = 0; (uintptr_t)&grid[gaddress(x,y,z,grid_size)] % 16 != 0 && z < grid_size; z++) {
							distance = pythagoras(atom->coord[1],
											atom->coord[2],
											atom->coord[3],
											centers[x], centers[y], centers[z]);
							if (distance < 2.0) distance = 2.0;

							if (distance <= 6.0) epsilon = 4;
							else if (distance >= 8.0) epsilon = 80;
							else epsilon = (38 * distance) - 224;
							grid[gaddress(x,y,z,grid_size)] += (fftwf_real)(atom->charge / (epsilon * distance));
						}

						for (; z < grid_size - 3; z += 16/sizeof(fftwf_real)) {
							__m128 z_centre = _mm_load_ps(&centers[z]);

							__m128 sub_x = _mm_sub_ps(atom_coord1, x_centre);
							__m128 sub_y = _mm_sub_ps(atom_coord2, y_centre);
							__m128 sub_z = _mm_sub_ps(atom_coord3, z_centre);

							__m128 mul_x = _mm_mul_ps(sub_x, sub_x);
							__m128 mul_y = _mm_mul_ps(sub_y, sub_y);
							__m128 mul_z = _mm_mul_ps(sub_z, sub_z);

							__m128 sum0 = _mm_add_ps(mul_x, mul_y);
							__m128 sum1 = _mm_add_ps(sum0, mul_z);

							__m128 distance = _mm_sqrt_ps(sum1);

							__m128 cmp1 = _mm_cmplt_ps(distance, const_2_0);
							distance = _mm_or_ps(_mm_and_ps(cmp1, const_2_0), _mm_andnot_ps(cmp1, distance));

							__m128 dist_cmpge8_0 = _mm_cmpge_ps(distance, const_8_0);
							__m128 dist_cmple6_0 = _mm_cmple_ps(distance, const_6_0);
							__m128 dist_else = _mm_xor_ps(_mm_or_ps(dist_cmpge8_0, dist_cmple6_0),
							                              const_neg1);

							__m128 dist_mulsub = _mm_sub_ps(_mm_mul_ps(const_38, distance), const_224);

							__m128 epsilon_ge8_0 = _mm_and_ps(dist_cmpge8_0, const_80);
							__m128 epsilon_le6_0 = _mm_and_ps(dist_cmple6_0, const_4);
							__m128 epsilon_else = _mm_and_ps(dist_else, dist_mulsub);

							__m128 epsilon = _mm_or_ps(_mm_or_ps(epsilon_ge8_0, epsilon_le6_0), epsilon_else);

							__m128 e_mul_dist = _mm_mul_ps(epsilon, distance);
							__m128 phi = _mm_div_ps(atom_charge, e_mul_dist);

							_mm_store_ps(&grid[gaddress(x,y,z,grid_size)], _mm_add_ps(_mm_load_ps(&grid[gaddress(x,y,z,grid_size)]), phi));
						}

						//Epilogue
						for (; z < grid_size; z++) {
							distance = pythagoras(atom->coord[1],
											atom->coord[2],
											atom->coord[3],
											centers[x], centers[y], centers[z]);
							if (distance < 2.0) distance = 2.0;

							if (distance <= 6.0) epsilon = 4;
							else if (distance >= 8.0) epsilon = 80;
							else epsilon = (38 * distance) - 224;
							grid[gaddress(x,y,z,grid_size)] += (fftwf_real)(atom->charge / (epsilon * distance));
						}
					}
				}
			}
		}
	}

	printf("\n");

	/************/

	return;

}



/************************/



void electric_point_charge( struct Structure This_Structure, float grid_span, int grid_size, fftwf_real *grid ) {

/************/

  /* Counters */

  int	residue, atom ;

  /* Co-ordinates */

  int	x, y, z ;
  int	x_low, x_high, y_low, y_high, z_low, z_high ;

  float		a, b, c ;
  float		x_corner, y_corner, z_corner ;
  float		w ;

  /* Variables */

  float		one_span ;

/************/

  for (x = 0; x < grid_size; x++) {
    for (y = 0; y < grid_size; y++) {
      for (z = 0; z < grid_size; z++) {

        grid[gaddress(x,y,z,grid_size)] = (fftwf_real)0;

      }
    }
  }

/************/

  one_span = grid_span / (float)grid_size;

  for (residue = 1; residue <= This_Structure.length; residue++) {
    for (atom = 1; atom <= This_Structure.Residue[residue].size; atom++) {

      if (This_Structure.Residue[residue].Atom[atom].charge != 0) {

        x_low = gord( This_Structure.Residue[residue].Atom[atom].coord[1] - (one_span / 2), grid_span, grid_size);
        y_low = gord( This_Structure.Residue[residue].Atom[atom].coord[2] - (one_span / 2), grid_span, grid_size);
        z_low = gord( This_Structure.Residue[residue].Atom[atom].coord[3] - (one_span / 2), grid_span, grid_size);

        x_high = x_low + 1;
        y_high = y_low + 1;
        z_high = z_low + 1;

        a = This_Structure.Residue[residue].Atom[atom].coord[1] - gcentre(x_low, grid_span, grid_size) - (one_span / 2);
        b = This_Structure.Residue[residue].Atom[atom].coord[2] - gcentre(y_low, grid_span, grid_size) - (one_span / 2);
        c = This_Structure.Residue[residue].Atom[atom].coord[3] - gcentre(z_low, grid_span, grid_size) - (one_span / 2);

        for (x = x_low; x <= x_high; x++) {

          x_corner = one_span * ((float)(x - x_high) + .5);

          for (y = y_low; y <= y_high; y++) {

            y_corner = one_span * ((float)(y - y_high) + .5 );

            for (z = z_low; z <= z_high; z++) {

              z_corner = one_span * ((float)(z - z_high) + .5);

              w = ((x_corner + a) * (y_corner + b) * (z_corner + c)) / ( 8.0 * x_corner * y_corner * z_corner);

              grid[gaddress(x, y, z, grid_size)] += (fftwf_real)(w * This_Structure.Residue[residue].Atom[atom].charge);

            }
          }
        }

      }

    }
  }

/************/

  return;

}



/************************/



void electric_field_zero_core( int grid_size, fftwf_real *elec_grid, fftwf_real *surface_grid, float internal_value ) {

/************/

  /* Co-ordinates */

  int	x, y, z;

/************/

  for (x = 0; x < grid_size; x++) {
    for (y = 0; y < grid_size; y++ ) {
      for (z = 0; z < grid_size; z++) {

        if (surface_grid[gaddress(x,y,z,grid_size)] == (fftwf_real)internal_value) elec_grid[gaddress(x,y,z,grid_size)] = (fftwf_real)0;

      }
    }
  }

/************/

  return;

}
