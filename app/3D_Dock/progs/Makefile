# Copyright (C) 1997-2000 Gidon Moont

# Biomolecular Modelling Laboratory
# Imperial Cancer Research Fund
# 44 Lincoln's Inn Fields
# London WC2A 3PX

# +44 (0)20 7269 3565
# http://www.bmm.icnet.uk/

#############

# This line you will definitely have to edit

FFTW_DIR        = ../../../lib/fftw-3.3.4/installation

#############

# You may need/want to edit some of these
#
# Hint: For the CC_FLAGS have a look at what the fftw build used

SHELL           = /bin/sh

CC              = gcc

CC_FLAGS        = -O3 -march=native -g -static
STRIP           = strip

CC_LINKERS      = -lm

SECURITY	= chmod 555

####################################################

# You should not be editing anything below here

CC_FLAGS_FULL	= -I$(FFTW_DIR)/include $(CC_FLAGS)
FFTW_LINKERS    = -L$(FFTW_DIR)/lib -lfftw3f

#############

.SUFFIXES:	.c .o

.c.o:
		$(CC) $(CC_FLAGS_FULL) -c $<

#############

LIBRARY_OBJECTS = manipulate_structures.o angles.o coordinates.o electrostatics.o grid.o qsort_scores.o

PROGRAMS = ftdock build randomspin

all:		$(PROGRAMS)

#############

ftdock:		ftdock.o $(LIBRARY_OBJECTS) structures.h
		$(CC) $(CC_FLAGS_FULL) -o $@ ftdock.o $(LIBRARY_OBJECTS) $(FFTW_LINKERS) $(CC_LINKERS)
#		$(STRIP) $@
		$(SECURITY) $@

#############

build:		build.o $(LIBRARY_OBJECTS) structures.h
		$(CC) $(CC_FLAGS) -o $@ build.o $(LIBRARY_OBJECTS) $(CC_LINKERS)
#		$(STRIP) $@
		$(SECURITY) $@

#############

randomspin:	randomspin.o $(LIBRARY_OBJECTS) structures.h
		$(CC) $(CC_FLAGS) -o $@ randomspin.o $(LIBRARY_OBJECTS) $(CC_LINKERS)
#		$(STRIP) $@
		$(SECURITY) $@

#############

clean:
		rm -f *.o core $(PROGRAMS)

#############

# dependencies

ftdock.o:			structures.h
build.o:			structures.h
randomspin.o:			structures.h

angles.o:			structures.h
coordinates.o:			structures.h
electrostatics.o:		structures.h
grid.o:				structures.h
manipulate_structures.o:	structures.h
qsort_scores.o:			structures.h


# tests
test1: ftdock
	./ftdock -static ../../../inputs/2pka.parsed -mobile ../../../inputs/5pti.parsed > ../../../outputs/output.test1

operf_clk: ftdock
	mkdir -p profilings/clk
	operf --event=CPU_CLK_UNHALTED:750000 ./ftdock -static ../../../inputs/2pka.parsed -mobile ../../../inputs/5pti.parsed > /dev/null
	opannotate --source ./ftdock > profilings/clk/ann_clk_1
	opreport -l > profilings/clk/rep_clk_1

	operf --event=CPU_CLK_UNHALTED:750000 ./ftdock -static ../../../inputs/1hba.parsed -mobile ../../../inputs/5pti.parsed > /dev/null
	opannotate --source ./ftdock > profilings/clk/ann_clk_2
	opreport -l > profilings/clk/rep_clk_2

	operf --event=CPU_CLK_UNHALTED:750000 ./ftdock -static ../../../inputs/4hhb.parsed -mobile ../../../inputs/5pti.parsed > /dev/null
	opannotate --source ./ftdock > profilings/clk/ann_clk_3
	opreport -l > profilings/clk/rep_clk_3

	operf --event=CPU_CLK_UNHALTED:750000 ./ftdock -static ../../../inputs/1ACB_rec.parsed -mobile ../../../inputs/1ACB_lig.parsed > /dev/null
	opannotate --source ./ftdock > profilings/clk/ann_clk_4
	opreport -l > profilings/clk/rep_clk_4


operf_tlb: ftdock
	mkdir -p profilings/tlb
	operf --event=tlb_access:1000000 ./ftdock -static ../../../inputs/2pka.parsed -mobile ../../../inputs/5pti.parsed > /dev/null

	operf --event=tlb_access:1000000 ./ftdock -static ../../../inputs/2pka.parsed -mobile ../../../inputs/5pti.parsed > /dev/null
	opannotate --source ./ftdock > profilings/tlb/ann_tlb_1
	opreport -l > profilings/tlb/rep_tlb_1

	operf --event=tlb_access:1000000 ./ftdock -static ../../../inputs/1hba.parsed -mobile ../../../inputs/5pti.parsed > /dev/null
	opannotate --source ./ftdock > profilings/tlb/ann_tlb_2
	opreport -l > profilings/tlb/rep_tlb_2

	operf --event=tlb_access:1000000 ./ftdock -static ../../../inputs/4hhb.parsed -mobile ../../../inputs/5pti.parsed > /dev/null
	opannotate --source ./ftdock > profilings/tlb/ann_tlb_3
	opreport -l > profilings/tlb/rep_tlb_3

	operf --event=tlb_access:1000000 ./ftdock -static ../../../inputs/1ACB_rec.parsed -mobile ../../../inputs/1ACB_lig.parsed > /dev/null
	opannotate --source ./ftdock > profilings/tlb/ann_tlb_4
	opreport -l > profilings/tlb/rep_tlb_4

operf_llc: ftdock
	mkdir -p profilings/llc
	operf --event=LLC_MISSES:6000 ./ftdock -static ../../../inputs/2pka.parsed -mobile ../../../inputs/5pti.parsed > /dev/null

	operf --event=LLC_MISSES:6000 ./ftdock -static ../../../inputs/2pka.parsed -mobile ../../../inputs/5pti.parsed > /dev/null
	opannotate --source ./ftdock > profilings/llc/ann_llc_1
	opreport -l > profilings/llc/rep_llc_1

	operf --event=LLC_MISSES:6000 ./ftdock -static ../../../inputs/1hba.parsed -mobile ../../../inputs/5pti.parsed > /dev/null
	opannotate --source ./ftdock > profilings/llc/ann_llc_2
	opreport -l > profilings/llc/rep_llc_2

	operf --event=LLC_MISSES:6000 ./ftdock -static ../../../inputs/4hhb.parsed -mobile ../../../inputs/5pti.parsed > /dev/null
	opannotate --source ./ftdock > profilings/llc/ann_llc_3
	opreport -l > profilings/llc/rep_llc_3

	operf --event=LLC_MISSES:6000 ./ftdock -static ../../../inputs/1ACB_rec.parsed -mobile ../../../inputs/1ACB_lig.parsed > /dev/null
	opannotate --source ./ftdock > profilings/llc/ann_llc_4
	opreport -l > profilings/llc/rep_llc_4

timing: ftdock
	mkdir -p timings
	./ftdock -static ../../../inputs/2pka.parsed -mobile ../../../inputs/5pti.parsed > /dev/null 2> timings/1
	./ftdock -static ../../../inputs/1hba.parsed -mobile ../../../inputs/5pti.parsed > /dev/null 2> timings/2
	./ftdock -static ../../../inputs/4hhb.parsed -mobile ../../../inputs/5pti.parsed > /dev/null 2> timings/3
	./ftdock -static ../../../inputs/1ACB_rec.parsed -mobile ../../../inputs/1ACB_lig.parsed > /dev/null 2> timings/4
